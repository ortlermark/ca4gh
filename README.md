# [Cellular Automate for Grasshopper](https://markortler.wordpress.com/gh/ca4gh/) #


CA4Gh is a framework for **string-expression-based neighborhood-generation**; a CELLULAR AUTOMATE.

This utility uses the **gh-internal expression-parser**; expanding it by pointing-operators and a **'Key-Value'-Database**:

* T{...}      OR      Top{...}
* L{...}      OR      Left{...}
* B{...}      OR      Bottom{...}
* R{...}      OR      Right{...}
* TL{...}      OR      TopLeft{...}
* TR{...}      OR      TopRight{...}
* BL{...}      OR      BottomLeft{...}
* BR{...}      OR      BottomRight{...}
* nesting like:    Top{... + Left{...}}


* all other variables are interpreted as a 'Key' and collected from the cell's database.
*(detected from the gh-internal parser)*