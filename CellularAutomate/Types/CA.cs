﻿using CellularAutomate.Parser;
using Grasshopper.Kernel.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CellularAutomate.Types
{
    public class CA : ICloneable
    {
        private readonly CA_Item[,] cells;

        public CA(int U, int V)
        {
            cells = new CA_Item[U, V];

            for (int x = 0; x < U; ++x)
                for (int y = 0; y < V; ++y)
                    cells[x, y] = new CA_Item();
        }

        public CA (CA other)
        {
            int U = other.cells.GetLength(0),
                V = other.cells.GetLength(1);

            cells = new CA_Item[U, V];

            other.iterateCells(CloneCells);
        }

        public object Clone()
        {
            return new CA(this);
        }

        private void CloneCells (CA_Item i, Tuple<int,int> idx)
        {
            var c = i.Clone() as CA_Item;
            cells[idx.Item1, idx.Item2] = c;
        }

        public void iterateCells(Action<CA_Item, Tuple<int,int>> callback)
        {
            for (int x = 0; x < cells.GetLength(0); x++)
                for (int y = 0; y < cells.GetLength(1); y++)
                    callback.Invoke(cells[x, y], Tuple.Create(x, y));
        }

        public CA_Item this[Tuple<int, int> idx]
        {
            get
            {
                int w = cells.GetLength(0),
                    h = cells.GetLength(1);

                int x = idx.Item1 % w,
                    y = idx.Item2 % h;

                return cells[x,y];
            }
        }

        public Tuple<int, int> Coordinates(CA_Item item)
        {
            int w = cells.GetLength(0);
            int h = cells.GetLength(1);

            for (int x = 0; x < w; ++x)
            {
                for (int y = 0; y < h; ++y)
                {
                    if (cells[x, y].Equals(item))
                        return Tuple.Create(x, y);
                }
            }

            return Tuple.Create(-1, -1);
        }

        #region relative
        public CA_Item Top(CA_Item centerCell) { return relative(centerCell, 0, 1); }
        public CA_Item TopLeft(CA_Item centerCell) { return relative(centerCell, -1, 1); }
        public CA_Item Left(CA_Item centerCell) { return relative(centerCell, -1, 0); }
        public CA_Item BottomLeft(CA_Item centerCell) { return relative(centerCell, -1, -1); }
        public CA_Item Bottom(CA_Item centerCell) { return relative(centerCell, 0, -1); }
        public CA_Item BottomRight(CA_Item centerCell) { return relative(centerCell, 1, -1); }
        public CA_Item Right(CA_Item centerCell) { return relative(centerCell, 1, 0); }
        public CA_Item TopRight(CA_Item centerCell) { return relative(centerCell, 1, 1); }

        private CA_Item relative(CA_Item centerCell, int dx, int dy)
        {
            Tuple<int, int> center = Coordinates(centerCell);

            int x = center.Item1, y = center.Item2;

            int w = cells.GetLength(0) - 1;
            int h = cells.GetLength(1) - 1;

            x = x + dx;
            y = y + dy;

            if (x < 0)
                x += w;

            if (x > w)
                x -= w;

            if (y < 0)
                y += h;

            if (y > h)
                y -= h;

            return cells[x, y];
        }
        #endregion

        public CA NextGeneration(CA_Expression solver)
        {
            var nxtGen = new CA(this);
            iterateCells((item, idx) => solveWith(nxtGen, solver, solver.ResultKey, item, idx));
            return nxtGen;
        }

        private void solveWith(CA nxtGen, CA_Expression solver, string DK, CA_Item item, Tuple<int, int> idx)
        {
            GH_Variant result = solver.Solve(new DataPass() { Root = this, item = item, index = idx });
            nxtGen[idx].Data[DK] = result;
        }

        public override string ToString()
        {
            return string.Format("CA(U:{0}, V:{1})", cells.GetLength(0), cells.GetLength(1));
        }
    }
}
