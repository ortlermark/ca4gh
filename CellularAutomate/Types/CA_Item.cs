﻿using Grasshopper.Kernel.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CellularAutomate.Types
{
    public class CA_Item : ICloneable
    {
        public readonly Dictionary<string, GH_Variant> Data;

        public CA_Item()
        {
            Data = new Dictionary<string, GH_Variant>();
        }
        public CA_Item(CA_Item other)
        {
            Data = new Dictionary<string, GH_Variant>();

            foreach (var kv in other.Data)
                Data.Add(kv.Key, kv.Value.Duplicate());
        }

        public object Clone()
        {
            return new CA_Item(this);
        }
    }
}
