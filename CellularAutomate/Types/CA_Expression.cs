﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grasshopper.Kernel.Expressions;
using CellularAutomate.Parser;

namespace CellularAutomate.Types
{
    public class DataPass
    {
        public CA Root;
        public CA_Item item;
        public Tuple<int,int> index;
    }

    public class CA_Expression : ExpressionResolver_Variant.ParserBase<DataPass>
    {
        private readonly HashSet<string> CaOperator = new HashSet<string>()
        {
            "BOTTOMLEFT", "BL",
            "BOTTOM", "B",
            "BOTTOMRIGHT", "BR",
            "RIGHT", "R",
            "TOPRIGHT", "TR",
            "TOP", "T",
            "TOPLEFT", "TL",
            "LEFT", "L",
        };

        private readonly HashSet<string> CaKeyWord = new HashSet<string>()
        {

        };

        public string Expression { get; private set; }
        public string ResultKey { get; internal set; }

        public CA_Expression() : base() { }

        public CA_Expression(string resultKey, string expression)
            : base(expression)
        {
            Expression = expression;
            ResultKey = resultKey;
        }

        protected override DataPass CastFromObject(object toCast)
        {
            var r = toCast as DataPass;
            if (r != null)
                return r;

            throw new NotSupportedException();
        }

        protected override bool isKeyword(string check, string rootOperator)
        {
            var k = check.ToUpper();
            return CaKeyWord.Contains(k);
        }

        protected override bool isOperator(string check, string rootOperator)
        {
            var k = check.ToUpper();
            return CaOperator.Contains(k);
        }

        protected override object ProcessOperator(DataPass operand, string toProcess, string rootOperator)
        {
            string op = toProcess.ToUpper();

            if (op == "BOTTOMLEFT" || op == "BL")
                return new DataPass() { Root = operand.Root, item = operand.Root.BottomLeft(operand.item) };
            else if (op == "BOTTOM" || op == "B")
                return new DataPass() { Root = operand.Root, item = operand.Root.Bottom(operand.item) };
            else if (op == "BOTTOMRIGHT" || op == "BR")
                return new DataPass() { Root = operand.Root, item = operand.Root.BottomRight(operand.item) };
            else if (op == "RIGHT" || op == "R")
                return new DataPass() { Root = operand.Root, item = operand.Root.Right(operand.item) };
            else if (op == "TOPRIGHT" || op == "TR")
                return new DataPass() { Root = operand.Root, item = operand.Root.TopRight(operand.item) };
            else if (op == "TOP" || op == "T")
                return new DataPass() { Root = operand.Root, item = operand.Root.Top(operand.item) };
            else if (op == "TOPLEFT" || op == "TL")
                return new DataPass() { Root = operand.Root, item = operand.Root.TopLeft(operand.item) };
            else if (op == "LEFT" || op == "L")
                return new DataPass() { Root = operand.Root, item = operand.Root.Left(operand.item) };
            else
                throw new NotSupportedException();
        }

        protected override GH_Variant ProcessOperatorResult(GH_Variant result, string evaluated, string rootOperator)
        {
            return result;
        }

        protected override GH_Variant ProcessKeyword(DataPass operand, string toProcess, string rootOperator)
        {
            throw new NotImplementedException();
        }

        protected override GH_Variant ProvideValue(DataPass operand, string toProcess, string rootOperator)
        {
            return operand.item.Data[toProcess];
        }
    }
}
