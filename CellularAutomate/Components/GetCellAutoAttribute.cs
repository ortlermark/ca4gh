﻿using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using CellularAutomate.Parameters;
using Grasshopper.Kernel.Data;
using CellularAutomate.Types;
using Grasshopper.Kernel.Expressions;
using Grasshopper.Kernel.Types;

namespace CellularAutomate.Components
{
    public class GetCellAutoAttribute : GH_Component
    {
        public GetCellAutoAttribute()
          : base("GetAttribute", "CAGA",
              "Get an attribute by Index, Key and Value",
              "Extra", "Cellular Automate")
        {
        }

        public override bool IsPreviewCapable { get { return false; } }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddParameter(new CA_Param(), "Cellular Automate", "CA", "expression based Cellular Automate", GH_ParamAccess.item);
            pManager.AddPathParameter("Cell-Index", "ci", "2d-Path", GH_ParamAccess.list);
            pManager.AddTextParameter("DataKey", "DK", "", GH_ParamAccess.list);
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("DataValue", "DV", "Value", GH_ParamAccess.list);
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            CA_Data cd = new CA_Data();
            DA.GetData(0, ref cd);

            cd = cd.Duplicate() as CA_Data;

            List<GH_Path> Pa = new List<GH_Path>();
            DA.GetDataList(1, Pa);

            List<string> keys = new List<string>();
            DA.GetDataList(2, keys);

            int kc = keys.Count;
            int pc = Pa.Count;

            int mc = Math.Max(kc, pc);

            List<IGH_Goo> values = new List<IGH_Goo>();
            for (int i = 0; i < mc; i++)
            {
                string k = keys[i % kc];
                int[] idx = Pa[i % pc].Indices;
                GH_Variant val;

                if (cd.Value[Tuple.Create(idx[0], idx[1])].Data.TryGetValue(k, out val))
                    values.Add(val.ToGoo());
                else
                    values.Add(null);
            }

            DA.SetDataList(0, values);
        }

        private GH_Variant CastToVariant(object ob)
        {
            var v = ob as IGH_Goo;
            

            bool bv;
            if (v.CastTo(out bv))
                return new GH_Variant(bv);

            double dv;
            if (v.CastTo(out dv))
                return new GH_Variant(dv);

            Complex cv;
            if (v.CastTo(out cv))
                return new GH_Variant(cv);

            int iv;
            if (v.CastTo(out iv))
                return new GH_Variant(iv);

            Plane pv;
            if (v.CastTo(out pv))
                return new GH_Variant(pv);

            Point3d p3v;
            if (v.CastTo(out p3v))
                return new GH_Variant(p3v);

            string sv;
            if (v.CastTo(out sv))
                return new GH_Variant(sv);

            Vector3d vv;
            if (v.CastTo(out vv))
                return new GH_Variant(vv);

            throw new NotSupportedException();
        }

        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Properties.Resources.Icons_Get;
            }
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{DB60A2E5-548D-4484-8F8B-06E863DB997E}"); }
        }
    }
}