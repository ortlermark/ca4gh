﻿using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using CellularAutomate.Parameters;
using CellularAutomate.Parser;

namespace CellularAutomate.Components
{
    public class RunCellAuto : GH_Component
    {
        public override Guid ComponentGuid { get { return new Guid("{756c586f-ebeb-4237-a5bd-41d63472d21f}"); } }

        public RunCellAuto()
          : base("CellularAutomate", "CA",
              "Run cellular-automate with given expression.",
              "Extra", "Cellular Automate")
        {
        }
        public override bool IsPreviewCapable { get { return false; } }

        protected override void RegisterInputParams(GH_InputParamManager pManager)
        {
            pManager.AddParameter(new CA_Param(), "Cellular Automate", "CA", "expression based Cellular Automate", GH_ParamAccess.item);
            pManager.AddParameter(new CA_Expression_Param(), "CA_Expression", "CAEx", "", GH_ParamAccess.item);
            pManager.AddIntegerParameter("Generations", "n", "run n-times", GH_ParamAccess.item, 1);
        }

        protected override void RegisterOutputParams(GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new CA_Param(), "Cellular Automate", "CA", "expression based Cellular Automate", GH_ParamAccess.list);
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            CA_Data cd = new CA_Data();
            DA.GetData(0, ref cd);

            CA_Expression_Data solver = new CA_Expression_Data();
            DA.GetData(1, ref solver);

            int n = 1;
            DA.GetData(2, ref n);

            var cdnxt = cd.Value;
            List<CA_Data> results = new List<CA_Data>() { cd.Duplicate() as CA_Data };
            for (int i = 0; i < n; i++)
            {
                cdnxt = cdnxt.NextGeneration(solver.Value);
                var res = new CA_Data() { Value = cdnxt };
                results.Add(res);
            }
            
            DA.SetDataList(0, results);
        }

        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Properties.Resources.Icons_Run_n;
            }
        }
    }
}
