﻿using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using CellularAutomate.Parameters;
using CellularAutomate.Parser;
using System.Linq;

namespace CellularAutomate.Components
{
    public class RunCellAutoSeq : GH_Component
    {
        public override Guid ComponentGuid { get { return new Guid("{D20B5D3A-DB27-4F11-A522-907BA2BEE32A}"); } }

        public RunCellAutoSeq()
          : base("CellularAutomateSeq", "CAS", "Run cellular-automate with given expressions in sequential order.", "Extra", "Cellular Automate")
        {
        }

        public override bool IsPreviewCapable { get { return false; } }

        protected override void RegisterInputParams(GH_InputParamManager pManager)
        {
            pManager.AddParameter(new CA_Param(), "Cellular Automate", "CA", "expression based Cellular Automate", GH_ParamAccess.item);
            pManager.AddParameter(new CA_Expression_Param(), "CA_Expression", "CAEx", "Sequence of expressions", GH_ParamAccess.list);
        }

        protected override void RegisterOutputParams(GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new CA_Param(), "Cellular Automate", "CA", "expression based Cellular Automate", GH_ParamAccess.list);
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            CA_Data cd = new CA_Data();
            DA.GetData(0, ref cd);

            List<CA_Expression_Data> solver = new List<CA_Expression_Data>();
            DA.GetDataList(1, solver);

            var cdnxt = cd.Value;
            List<CA_Data> results = new List<CA_Data>() { cd.Duplicate() as CA_Data };

            foreach (var sol in solver)
            {
                cdnxt = cdnxt.NextGeneration(sol.Value);
                var res = new CA_Data() { Value = cdnxt };
                results.Add(res);
            }
            
            DA.SetDataList(0, results);
        }

        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Properties.Resources.Icons_Run_s;
            }
        }
    }
}
