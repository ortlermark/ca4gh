﻿using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using CellularAutomate.Parameters;
using CellularAutomate.Types;
using System.Text.RegularExpressions;

namespace CellularAutomate.Components
{
    public class CreateCellAutoExpression : GH_Component
    {
        public CreateCellAutoExpression()
          : base("Create Expression", "CEx",
              "Parse the given string to an expression for the use with cellular automate.",
              "Extra", "Cellular Automate")
        {
        }

        public override bool IsPreviewCapable { get { return false; } }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddTextParameter("Expression", "ex", "Operators: Upper, UpperLeft, Left, LowerLeft, Lower, LowerRight, Right, UpperRight", GH_ParamAccess.item);
            pManager.AddTextParameter("ResultKey", "rk", "The key where to write the result of the expression given in 'ex'", GH_ParamAccess.item);
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new CA_Expression_Param(), "CA_Expression", "CAEx", "Expression use for Cellular Automate", GH_ParamAccess.item);
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            string ex = "";
            string k = "";
            DA.GetData(0, ref ex);
            DA.GetData(1, ref k);

            if (!Regex.IsMatch(k, "[a-zA-Z]"))
                throw new NotSupportedException(string.Format("alpha only! Regex[a-zA-Z] != '{0}'", k));

            var r = new CA_Expression_Data() { Value = new CA_Expression(k, ex) };
            DA.SetData(0, r);
        }

        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Properties.Resources.Icons_Expression;
            }
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{bcf0e075-638d-4f25-9127-da3ca3b2ab94}"); }
        }
    }
}