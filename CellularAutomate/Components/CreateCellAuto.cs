﻿using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using CellularAutomate.Parameters;
using CellularAutomate.Parser;
using CellularAutomate.Types;
using Grasshopper.Kernel.Data;

namespace CellularAutomate.Components
{
    public class CreateCellAuto : GH_Component
    {
        public override Guid ComponentGuid { get { return new Guid("{E9AD8A62-AB0F-40BE-A594-9AB03E3C5E35}"); } }

        public CreateCellAuto()
          : base("CreateCellularAutomate", "CCA",
              "Creates a Cellular Automate with given UV-Dimension.",
              "Extra", "Cellular Automate")
        {
        }

        public override bool IsPreviewCapable { get { return false; } }

        protected override void RegisterInputParams(GH_InputParamManager pManager)
        {
            pManager.AddIntegerParameter("U-Dimension", "U", "Dimension in U-Direction", GH_ParamAccess.item, 10);
            pManager.AddIntegerParameter("V-Dimension", "V", "Dimension in V-Direction", GH_ParamAccess.item, 10);
        }

        protected override void RegisterOutputParams(GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new CA_Param(), "Cellular Automate", "CA", "expression based Cellular Automate", GH_ParamAccess.item);
            pManager.AddPathParameter("Cell-Indices", "ci", "All 2d-Indices of the Cellular Automate. U then V.", GH_ParamAccess.list);
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            int U = 1;
            DA.GetData(0, ref U);

            int V = 1;
            DA.GetData(1, ref V);


            DA.SetData(0, new CA_Data() { Value = new CA(U, V) });

            List<GH_Path> paths = new List<GH_Path>();
            for (int i = 0; i < U; i++)
                for (int j = 0; j < V; j++)
                    paths.Add(new GH_Path(i, j));

            DA.SetDataList(1, paths);
        }

        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Properties.Resources.Icons_Blank;
            }
        }
    }
}
