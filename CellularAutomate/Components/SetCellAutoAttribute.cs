﻿using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using CellularAutomate.Parameters;
using Grasshopper.Kernel.Data;
using CellularAutomate.Types;
using Grasshopper.Kernel.Expressions;
using Grasshopper.Kernel.Types;
using System.Text.RegularExpressions;

namespace CellularAutomate.Components
{
    public class SetCellAutoAttribute : GH_Component
    {
        public SetCellAutoAttribute()
          : base("SetAttribute", "CASA",
              "Set an attribute by Index, Key and Value",
              "Extra", "Cellular Automate")
        {
        }

        public override bool IsPreviewCapable { get { return false; } }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddParameter(new CA_Param(), "Cellular Automate", "CA", "expression based Cellular Automate", GH_ParamAccess.item);
            pManager.AddPathParameter("Cell-Index", "ci", "2d-Path", GH_ParamAccess.list);
            pManager.AddTextParameter("DataKey", "DK", "Key (alpha only - regex([a-zA-Z])", GH_ParamAccess.list);
            pManager.AddGenericParameter("DataValue", "DV", "Value", GH_ParamAccess.list);
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new CA_Param(), "Cellular Automate", "CA", "expression based Cellular Automate", GH_ParamAccess.item);
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            CA_Data cd = new CA_Data();
            DA.GetData(0, ref cd);

            cd = cd.Duplicate() as CA_Data;

            List<GH_Path> Pa = new List<GH_Path>();
            DA.GetDataList(1, Pa);

            List<string> keys = new List<string>();
            DA.GetDataList(2, keys);

            List<object> values = new List<object>();
            DA.GetDataList(3, values);

            int kc = keys.Count,
                vc = values.Count;

            if (kc != vc)
                throw new NotSupportedException(string.Format("DataKey(n={0}) and DataValue(n={1}) has to be from same length", kc, vc));

            int pc = Pa.Count;

            int mc = Math.Max(kc, pc);

            for (int i = 0; i < mc; i++)
            {
                string k = keys[i % kc];

                if (!Regex.IsMatch(k, "[a-zA-Z]"))
                    throw new NotSupportedException(string.Format("alpha only! Regex[a-zA-Z] != '{0}'", k));

                GH_Variant v = CastToVariant(values[i % vc]);

                int[] idx = Pa[i % pc].Indices;

                cd.Value[Tuple.Create(idx[0], idx[1])].Data[k] = v;
            }

            DA.SetData(0, cd);
        }

        private GH_Variant CastToVariant(object ob)
        {
            var v = ob as IGH_Goo;
            

            bool bv;
            if (v.CastTo(out bv))
                return new GH_Variant(bv);

            double dv;
            if (v.CastTo(out dv))
                return new GH_Variant(dv);

            Complex cv;
            if (v.CastTo(out cv))
                return new GH_Variant(cv);

            int iv;
            if (v.CastTo(out iv))
                return new GH_Variant(iv);

            Plane pv;
            if (v.CastTo(out pv))
                return new GH_Variant(pv);

            Point3d p3v;
            if (v.CastTo(out p3v))
                return new GH_Variant(p3v);

            string sv;
            if (v.CastTo(out sv))
                return new GH_Variant(sv);

            Vector3d vv;
            if (v.CastTo(out vv))
                return new GH_Variant(vv);

            throw new NotSupportedException();
        }

        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Properties.Resources.Icons_Set;
            }
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{beb2a8c4-1296-47ee-a34b-84d52fdd8153}"); }
        }
    }
}