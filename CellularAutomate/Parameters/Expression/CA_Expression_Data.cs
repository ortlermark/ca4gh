﻿using System;
using Grasshopper.Kernel.Types;
using CellularAutomate.Parser;
using CellularAutomate.Types;

namespace CellularAutomate.Parameters
{
    public class CA_Expression_Data : GH_Goo<CA_Expression>
    {
        public CA_Expression_Data()
        {
            Value = new CA_Expression();
        }

        public override bool IsValid
        {
            get { return Value.Expression != string.Empty && Value.ResultKey != string.Empty; }
        }

        public override string TypeDescription
        {
            get
            {
                return "Expression used with Cellular Automate";
            }
        }

        public override string TypeName
        {
            get
            {
                return "Cellular Automate Expression";
            }
        }

        public override IGH_Goo Duplicate()
        {
            return new CA_Expression_Data() { Value = new CA_Expression(Value.ResultKey, Value.Expression) };
        }

        public override string ToString()
        {
            return string.Format("CA_Expression ( {0} <= {1} )", Value.ResultKey, Value.Expression);
        }
    }
}