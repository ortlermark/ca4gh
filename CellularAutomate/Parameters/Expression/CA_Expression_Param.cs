﻿using Grasshopper.Kernel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace CellularAutomate.Parameters
{
    public class CA_Expression_Param : GH_Param<CA_Expression_Data>
    {
        public CA_Expression_Param() 
            : base("CellularAutomateExpression_Param", "CAEx", "expression based Cellular Automate", "Extra", "Cellular Automate", GH_ParamAccess.item)
        {

        }
        public override Guid ComponentGuid { get { return new Guid("{61488642-F89F-4601-952A-4A65DB952D1D}"); } }

        protected override Bitmap Icon { get { return Properties.Resources.Icons_Expression; } }

        public override GH_Exposure Exposure { get { return GH_Exposure.hidden; } }
    }
}
