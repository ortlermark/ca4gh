﻿using Grasshopper.Kernel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace CellularAutomate.Parameters
{
    public class CA_Param : GH_Param<CA_Data>
    {
        public CA_Param()
            : base("CellularAutomate_Param", "CA", "expression based Cellular Automate", "Extra", "Cellular Automate", GH_ParamAccess.item)
        {

        }
        public override Guid ComponentGuid
        {
            get
            {
                return new Guid("{E85B46C8-CCE3-4DCC-BAF9-7F315F70647D}");
            }
        }

        protected override Bitmap Icon
        {
            get
            {
                return Properties.Resources.Icons_Blank;
            }
        }
    }
}
