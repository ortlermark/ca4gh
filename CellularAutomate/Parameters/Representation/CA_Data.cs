﻿using System;
using CellularAutomate.Types;
using Grasshopper.Kernel.Types;

namespace CellularAutomate
{
    public class CA_Data : GH_Goo<CA>
    {
        public CA_Data()
        {
            Value = null;
        }

        public CA_Data(CA_Data other)
        {
             Value = other.Value.Clone() as CA;
        }

        public override bool IsValid
        {
            get { return true; }
        }

        public override string TypeDescription { get { return "DataContainer for Cellular Automate"; } }

        public override string TypeName { get { return "CellularAutomate_Data"; } }

        public override IGH_Goo Duplicate()
        {
            return new CA_Data(this);
        }

        public override string ToString()
        {
            return Value.ToString();
        }
    }
}