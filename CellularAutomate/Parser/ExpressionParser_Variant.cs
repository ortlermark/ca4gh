﻿using Grasshopper.Kernel.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace CellularAutomate.Parser
{
    public static class ExpressionResolver_Variant
    {
        #region abstract
        public abstract class ParserBase<T>
        {
            private Func<object, GH_Variant> ExpressionCache;

            public ParserBase() {}

            protected ParserBase(string expression)
            {
                ExpressionCache = 
                    Parse<T>
                        (   
                            CastFromObject, 
                            expression, 
                            ProvideValue, 
                            ProcessOperator, 
                            ProcessOperatorResult, 
                            isOperator, 
                            ProcessKeyword, 
                            isKeyword,
                            SubParsing
                        );
            }

            #region abstract
            protected abstract T CastFromObject(object toCast);

            protected abstract object ProcessOperator(T operand, string toProcess, string rootOperator);
            protected abstract GH_Variant ProcessOperatorResult(GH_Variant result, string evaluated, string rootOperator);

            protected abstract GH_Variant ProcessKeyword(T operand, string toProcess, string rootOperator);

            protected abstract GH_Variant ProvideValue(T operand, string toProcess, string rootOperator);

            protected abstract bool isOperator(string check, string rootOperator);
            protected abstract bool isKeyword(string check, string rootOperator);

            protected virtual Func<object, GH_Variant> SubParsing(string toProcess, string subExpression)
            {
                //return null to let the recursion go on.
                return null;
            }

            #endregion
            public GH_Variant Solve(object with) { return ExpressionCache.Invoke(with); }
        }
        #endregion

        #region static
        public static GH_Variant Solve<T>
            (
                Func<object, T> cast,
                string expression,
                T solverBase,   
                Func<T, string, string, GH_Variant> valueProvider,

                Func<T, string, string, object> processOperator,
                Func<GH_Variant, string, string, GH_Variant> processOperatorResult,
                Func<string, string, bool> isOperator,

                Func<T, string,string, GH_Variant> processKeyword,
                Func<string, string, bool> isKeyword,

                Func<string, string, Func<object, GH_Variant>> leaveNestingWith = null
            )
        {
            var solver = Parse(cast, expression, valueProvider, processOperator, processOperatorResult, isOperator, processKeyword, isKeyword, leaveNestingWith);
            return solver.Invoke(solverBase);
        }

        public static Func<object,GH_Variant> Parse<T>
            (
                Func<object, T> cast,
                string expression,
                Func<T, string, string, GH_Variant> valueProvider,

                Func<T, string, string, object> processOperator,
                Func<GH_Variant, string,string,GH_Variant> processOperatorResult,
                Func<string,string, bool> isOperator,

                Func<T, string, string, GH_Variant> processKeyword,
                Func<string, string, bool> isKeyword,

                Func<string,string, Func<object, GH_Variant>> leaveNestingWith = null

            )
        {
            var r = new RecursivExpressionParser<T>(cast, expression, valueProvider, isOperator, processOperator, processOperatorResult, isKeyword, processKeyword, leaveNestingWith);
            return r.Evaluate;
        }
        #endregion

        private struct RecursivExpressionParser<T>
        {
            #region const
            private const char Opening = '{';
            private const char Closing = '}';
            private const char KeyWordMarker = '$';
            #endregion

            #region Fields
            private IEnumerator<string> UniqueVariableName;

            #region Instance
            private Dictionary<string, Tuple<string, Func<object, GH_Variant>>> Variable_SubFunctions;
            private Dictionary<string, string> Variable_KeyWord;

            private GH_ExpressionParser Parser;
            private HashSet<string> Symbols;

            private string RootOperator;
            #endregion

            #region go with recursion
            private Func<T, string, string, GH_Variant> ProvideValue;

            private Func<T, string, string, object> ProcessOperator;
            private Func<GH_Variant, string, string, GH_Variant> ProcessOperatorResult;

            private Func<T, string, string, GH_Variant> ProcessKeyword;

            private Func<string, string, bool> OperatorCheck;
            private Func<string, string, bool> KeywordCheck;
            private Func<object,T> CastFromObject;

            private Func<string, string, Func<object, GH_Variant>> LeaveNestingWith;
            #endregion
            #endregion

            #region Constructor
            private RecursivExpressionParser(RecursivExpressionParser<T> root, string expr, string rootOperator)
            {
                Variable_SubFunctions = new Dictionary<string, Tuple<string, Func<object, GH_Variant>>>();
                Variable_KeyWord = new Dictionary<string, string>();

                ProvideValue = root.ProvideValue;

                ProcessKeyword = root.ProcessKeyword;
                ProcessOperator = root.ProcessOperator;

                KeywordCheck = root.KeywordCheck;
                OperatorCheck = root.OperatorCheck;

                RootOperator = rootOperator;

                ProcessOperatorResult = root.ProcessOperatorResult;

                UniqueVariableName = null;
                Parser = null;
                Symbols = null;

                CastFromObject = root.CastFromObject;

                LeaveNestingWith = root.LeaveNestingWith;

                UniqueVariableName = PrepareUniqueVariableName(expr);

                ParseIt(expr);
            }

            public RecursivExpressionParser
                (
                    Func<object, T> cast,
                    string expr,

                    Func<T, string, string, GH_Variant> valueProvider,

                    Func<string, string, bool> isOperator,
                    Func<T, string, string, object> processOperator,
                    Func<GH_Variant,string,string,GH_Variant> processOperatorResult,

                    Func<string, string, bool> isKeyword,
                    Func<T, string, string, GH_Variant> processKeyword,

                    Func<string,string,Func<object,GH_Variant>> leaveNestingWith
                )
            {
                ProvideValue = valueProvider;

                OperatorCheck = isOperator;
                KeywordCheck = isKeyword;

                ProcessOperator = processOperator;
                ProcessKeyword = processKeyword;

                ProcessOperatorResult = processOperatorResult;

                Variable_SubFunctions = new Dictionary<string, Tuple<string, Func<object, GH_Variant>>>();
                Variable_KeyWord = new Dictionary<string, string>();

                RootOperator = "";

                UniqueVariableName = null;
                Parser = null;
                Symbols = null;

                CastFromObject = cast;

                LeaveNestingWith = leaveNestingWith;
                UniqueVariableName = PrepareUniqueVariableName(expr);

                ParseIt(expr);
            }

            private void ParseIt(string expr)
            {
                var r = expr;
                expr = ParseExpressionRecursivly(expr);

                var parser = new GH_ExpressionParser();

                if (!parser.CacheSymbols(expr))
                    throw new NotSupportedException("GH_ExpressionParser failed to cache the expressions symbols.");

                Parser = parser;

                var variables = parser.CachedSymbols().Where(x => x.m_class == GH_ParserTokenClass.Identifier).Select(x => x.m_token);

                Symbols = new HashSet<string>(variables);
            }
            #endregion

            #region VariableNaming
            private IEnumerator<string> PrepareUniqueVariableName(string key)
            {
                string allChars = string.Join("", key.Distinct());
                allChars = Regex.Replace(allChars, "[^a-zA-Z0-9]", "");

                int i = 0;
                while (true)
                {
                    yield return "C_" + allChars + i.ToString();
                    i++;
                }
            }

            private string GetUniqueVariableName()
            {
                UniqueVariableName.MoveNext();
                return UniqueVariableName.Current;
            }
            #endregion

            #region parsing
            private string ParseExpressionRecursivly(string ex)
            {
                int CharCount = 0;

                int lastNonAlpha = 0;
                int parenthesisOpenedAt = 0;
                int parenthesisBalance = 0;
                bool insideParenthesis = false;

                while (CharCount < ex.Count())
                {
                    char cc = ex[CharCount];

                    if (cc == Opening)
                    {
                        if (insideParenthesis)
                        {
                            parenthesisBalance++;
                        }
                        else
                        {
                            parenthesisBalance++;
                            parenthesisOpenedAt = CharCount;
                            insideParenthesis = true;
                        }
                    }
                    else if (cc == Closing)
                    {
                        if (!insideParenthesis)
                            throw new NotSupportedException("unbalanced parentheses: unmatched closing");

                        parenthesisBalance--;

                        if (parenthesisBalance == 0)
                        {

                            string funOperator = ex.Substring(lastNonAlpha, parenthesisOpenedAt - lastNonAlpha);

                            if (OperatorCheck(funOperator, RootOperator))
                            {
                                int bo = CharCount + 1;

                                string fun = ex.Substring(parenthesisOpenedAt, bo - parenthesisOpenedAt);

                                string start = ex.Substring(0, lastNonAlpha);
                                string end = ex.Substring(bo);

                                var V = GetUniqueVariableName();

                                string subExpr = fun.Substring(1, fun.Length - 2);

                                Variable_SubFunctions
                                    .Add
                                        (
                                            V,
                                            Tuple.Create<string,Func<object,GH_Variant>>
                                                (
                                                    funOperator,
                                                    GoSub(subExpr, funOperator)
                                                )
                                        );

                                ex = string.Format("{0} {1} ", start, V);
                                CharCount = ex.Length-1;

                                ex = ex + end;

                                lastNonAlpha = -1;
                                parenthesisOpenedAt = -1;

                                
                            }
                            else
                            {
                                throw new NotSupportedException(string.Format("'{0}' is not reconized as an operator.", funOperator));
                            }

                            insideParenthesis = false;
                        }
                    }
                    else if (!insideParenthesis && cc == KeyWordMarker)
                    {
                        string kw = ex.Substring(lastNonAlpha);

                        Match match = Regex.Match(kw, "\\" + KeyWordMarker + "[a-zA-Z]+");

                        string keyword = match.Groups[0].Value;

                        int kl = keyword.Length;
                        keyword = keyword.Substring(1);

                        if (KeywordCheck(keyword, RootOperator))
                        {
                            string start = ex.Substring(0, lastNonAlpha);
                            string end = ex.Substring(lastNonAlpha + kl);

                            string V = GetUniqueVariableName();
                            Variable_KeyWord[V] = keyword;

                            ex = string.Format("{0} {1} ", start, V);
                            CharCount = ex.Length - 1;

                            ex = ex + end;

                            lastNonAlpha = -1;
                            parenthesisOpenedAt = -1;
                        }
                        else
                        {
                            throw new NotSupportedException(string.Format("'{0}' is not reconized as a keyword.", keyword));
                        }
                    }
                    else if (!insideParenthesis && !Regex.IsMatch(cc.ToString(), "[a-zA-Z]"))
                    {
                        lastNonAlpha = CharCount + 1;
                    }

                    CharCount++;
                }

                if (parenthesisBalance != 0)
                    throw new NotSupportedException("unbalanced parentheses: unmatched opening");

                return ex;
            }

            private Func<object,GH_Variant> GoSub(string subExpr, string root)
            {
                Func<object, GH_Variant> sub = null;

                if (LeaveNestingWith != null)
                    sub = LeaveNestingWith(root, subExpr);

                if (sub == null)
                    sub = new RecursivExpressionParser<T>(this, subExpr, root).Evaluate;

                return sub;
            }
            #endregion

            #region solver
            public GH_Variant Evaluate(object with)
            {
                T from = CastFromObject.Invoke(with);

                foreach (string s in Symbols)
                {
                    GH_Variant val;
                    Tuple<string, Func<object,GH_Variant>> t;
                    string kv;

                    if (Variable_KeyWord.TryGetValue(s, out kv))
                    {
                        val = ProcessKeyword(from, kv, RootOperator);
                    }
                    else if (Variable_SubFunctions.TryGetValue(s, out t))
                    {
                        object o = ProcessOperator(from, t.Item1, RootOperator);
                        var ex = t.Item2;

                        val = ex(o);
                        val = ProcessOperatorResult(val, t.Item1, RootOperator);
                    }
                    else
                        val = ProvideValue(from, s, RootOperator);

                    Parser.AddVariableEx(s, val);
                }

                return Parser.Evaluate();
            }
            #endregion
        }
    }
}
