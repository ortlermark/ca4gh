﻿using System;
using System.Drawing;
using Grasshopper.Kernel;

namespace CellularAutomate
{
    public class CellularAutomateInfo : GH_AssemblyInfo
    {
        public override Guid Id { get { return new Guid("b6d4021a-fd1b-4c3c-abd1-7a9eef1dc330"); } }
        public override string Name { get { return "CellularAutomate"; } }
        public override Bitmap Icon { get { return Properties.Resources.Icons_Blank; } }
        public override string Description { get { return "Expression based Cellular Automate"; } }
        public override string AuthorName { get { return "Ortler Mark"; } }
        public override string AuthorContact { get { return "ortlermark@gmail.com"; } }
    }
}
